FROM maven:alpine AS build
COPY src /build/src
COPY pom.xml /build
RUN mvn -f /build/pom.xml --no-transfer-progress -Djar.finalName=mueckenlaser.jar package

FROM alpine:3.11
RUN apk --no-cache add openjdk8-jre
COPY --from=build /build/target/mueckenlaser.jar /app/mueckenlaser.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/app/mueckenlaser.jar"]
