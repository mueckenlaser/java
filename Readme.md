# Mückenlaser

## Hardware installation

### Requirements 

- Arduino IDE
- Arduino Code ()

- Java
- Java Code ()

- Mückenlaser Hardware

### Preparation

- Install ESP8266 Board in your Ardunio IDE (https://arduino-esp8266.readthedocs.io/en/latest/installing.html)
- Install ArduinoJSON library from Library Manager within Arduino IDE
- Prepare your WiFi Password and SSID (The ESP needs it to connect to the java application on your computer)

#### Arduino Code

- Set Arduino IDE to ESP8266 Generic Module
- Insert your SSID and Password in respective Variables in Arduino Code
- Insert IP Adress of the computer running the java application into the respective Variable in Arduino Code
- Compile and copy the code to the ESP8266 with a USB cable
- Look into Serial Monitor of the ESP on boot to read its IP Adress

#### Java Code

- Insert IP Adress of the ESP into application.yml
- Connect the three microphones to your computer and name them each with a different name
- Insert microphone names
