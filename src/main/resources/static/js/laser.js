import {OrbitControls} from "./OrbitalControls.js";
import {
    Geometry,
    BoxGeometry,
    BufferGeometry,
    DirectionalLight,
    DoubleSide,
    Line,
    LineBasicMaterial,
    LineDashedMaterial,
    LineSegments,
    Mesh,
    MeshBasicMaterial,
    MeshPhongMaterial,
    Object3D,
    PerspectiveCamera,
    PlaneBufferGeometry,
    Scene,
    SphereGeometry,
    Vector3,
    WebGLRenderer
} from "./three.module.js";

/*
 * global variable to hold timer id
 * this is used to start and stop an interval timer which repeatedly calls
 * the getESPStatus() function in an specified interval
 */
let timerId = null;

/*
 * global variable to hold the main object in the three js scene
 */
let room = new Object3D();

window.onload = function () {
    console.log("beforedraw");
    draw_stuff();
    console.log("afterdraw");
    startESPStatusRefreshInterval();
    setupEventListeners();
};

function draw_stuff() {
    console.log("draw");
    let container = document.getElementById('three_js_container');
    let width = $(container).width();
    let height = $(container).height();

    let scene = new Scene();
    let camera = new PerspectiveCamera(75, width / height, 0.001, 1000);

    let renderer = new WebGLRenderer();
    renderer.setSize(width, height);
    container.appendChild(renderer.domElement);

    scene.add(room);

    // add light to the scene
    let light = new DirectionalLight(0xffffff);
    light.position.set(0, 1, 1).normalize();
    room.add(light);

    // add coordinate axes
    let axes = buildAxes(100);
    room.add(axes);

    let geometry = new PlaneBufferGeometry(2000, 2000, 8, 8);
    let material = new MeshPhongMaterial({color: 0x202020, side: DoubleSide});
    let plane = new Mesh(geometry, material);
    plane.position.set(0, 0, -0.01);
    room.add(plane);

    // add laser turret
    let radius = 0.04;
    let color = 0xff0000;
    let position = [0, 0, 0];
    let laser = createSphere(radius, color, position);
    room.add(laser);

    // add microphone0
    radius = 0.02;
    position = [0.577, 0, 0];
    color = 0x00ff00;
    let mic0 = createSphere(radius, color, position);
    room.add(mic0);

    // add microphone1
    position = [-0.289, 0.5, 0];
    let mic1 = createSphere(radius, color, position);
    room.add(mic1);

    // add microphone2
    position = [-0.289, -0.5, 0];
    let mic2 = createSphere(radius, color, position);
    room.add(mic2);

    // add vertical connection between microphones
    geometry = new BoxGeometry(0.86603, 0.02, 0.02);
    material = new MeshBasicMaterial({color: 0x8b4513});
    let cube = new Mesh(geometry, material);
    cube.position.x = 0.144335;
    room.add(cube);

    // add horizontal connection between microphones
    geometry = new BoxGeometry(0.02, 1, 0.02);
    material = new MeshBasicMaterial({color: 0x8b4513});
    cube = new Mesh(geometry, material);
    cube.position.x = -0.289;
    room.add(cube);

    // add laser beam
    let point0 = new Vector3(0, 0, 0);
    let point1 = new Vector3(2, 2, 2);
    color = 0xff0000;
    let laser_beam = createLine(point0, point1, color);
    laser_beam.name = "laser_beam";
    room.add(laser_beam);

    camera.position.set(1, 1, 0.4);
    camera.lookAt(laser);
    camera.up.set(0, 0, 1);
    let controls = new OrbitControls(camera, renderer.domElement);

    let animate = function () {
        requestAnimationFrame(animate);
        renderer.render(scene, camera);
    };

    animate();
}

// create a sphere with three.js
function createSphere(radius, color, position) {
    let geometry = new SphereGeometry(radius);
    let material = new MeshPhongMaterial({color: color});
    let sphere = new Mesh(geometry, material);
    sphere.position.x = position[0];
    sphere.position.y = position[1];
    sphere.position.z = position[2];
    return sphere;
}

// create a line from point0 to point1
function createLine(point0, point1, color) {
    let material = new LineBasicMaterial({
        color: color
    });
    let points = [];
    points.push(point0);
    points.push(point1);

    let geometry = new BufferGeometry().setFromPoints(points);
    let line = new Line(geometry, material);
    return line;
}

function buildAxes(length) {
    let axes = new Object3D();
    axes.add(buildAxis(new Vector3(0, 0, 0), new Vector3(length, 0, 0), 0xFF0000, false)); // +X
    axes.add(buildAxis(new Vector3(0, 0, 0), new Vector3(-length, 0, 0), 0xFF0000, true)); // -X
    axes.add(buildAxis(new Vector3(0, 0, 0), new Vector3(0, length, 0), 0x00FF00, false)); // +Y
    axes.add(buildAxis(new Vector3(0, 0, 0), new Vector3(0, -length, 0), 0x00FF00, true)); // -Y
    axes.add(buildAxis(new Vector3(0, 0, 0), new Vector3(0, 0, length), 0x0000FF, false)); // +Z
    axes.add(buildAxis(new Vector3(0, 0, 0), new Vector3(0, 0, -length), 0x0000FF, true)); // -Z
    return axes;
}

function buildAxis(src, dst, colorHex, dashed) {
    let geom = new Geometry(),
        mat;
    if (dashed) {
        mat = new LineDashedMaterial({linewidth: 1, color: colorHex, dashSize: 0.05, gapSize: 0.05});
    } else {
        mat = new LineBasicMaterial({linewidth: 1, color: colorHex});
    }
    geom.vertices.push(src.clone());
    geom.vertices.push(dst.clone());
    let axis = new Line(geom, mat, LineSegments);
    axis.computeLineDistances(); // This one is SUPER important, otherwise dashed lines will appear as simple plain lines
    return axis;
}

function updateThreeJs(resJSON) {
    let pitch = resJSON.pitch;
    let yaw = resJSON.yaw;
    let laser_beam = room.getObjectByName("laser_beam");
    let points = [];
    points.push(new Vector3(0, 0, 0));
    points.push(calculateUnitVector(pitch, yaw));
    laser_beam.geometry = new BufferGeometry().setFromPoints(points);
}

function calculateUnitVector(pitch, yaw) {
    pitch = (pitch - 90) * Math.PI / 180;
    yaw = yaw * Math.PI / 180;
    let x = Math.cos(yaw) * Math.cos(pitch);
    let y = Math.sin(yaw) * Math.cos(pitch);
    let z = Math.sin(pitch);
    return new Vector3(x, y, z);
}

// update inner HTML of Element with given ID
function updateInnerHTML(element, val) {
    if (typeof element === 'string') {
        element = document.getElementById(element)
    }
    element.innerHTML = val;
}

function replace_class(element, replace_this, replace_with) {
    element.classList.replace(replace_this, replace_with);
}

// stop the old interval timer and start a new interval
function startESPStatusRefreshInterval() {
    getESPStatus();
    clearInterval(timerId);
    timerId = setInterval(() => getESPStatus(), document.getElementById("auto_refresh_range").value * 1000);
}

function updateBadgeClasses(resJSON, pitchElement, yawElement, masterElement, pitchControlElement, yawControlElement, espConnectionStatusElement) {
    swapClassOfElementOnCondition(resJSON.pitch !== resJSON.pitch_control, pitchElement, "badge-success", "badge-warning");
    swapClassOfElementOnCondition(resJSON.yaw !== resJSON.yaw_control, yawElement, "badge-success", "badge-warning");
    swapClassOfElementOnCondition(resJSON.reaching_master === true, masterElement, "badge-danger", "badge-success");
    swapClassOfElementOnCondition(resJSON.reachable_by_master === true, espConnectionStatusElement, "badge-danger", "badge-success");

    // JSON-value is a String, input value is a number, therefore no strict comparison
    swapClassOfElementOnCondition(resJSON.pitch_control != document.getElementById("pitch_range").value, pitchControlElement, "badge-success", "badge-warning");

    // JSON-value is a String, input value is a number, therefore no strict comparison
    swapClassOfElementOnCondition(resJSON.yaw_control != document.getElementById("yaw_range").value, yawControlElement, "badge-success", "badge-warning");
}

// get Status from ESP and update table
async function getESPStatus() {
    let pitchElement = document.getElementById("esp_status_pitch");
    let yawElement = document.getElementById("esp_status_yaw");
    let masterElement = document.getElementById("esp_status_master");
    let pitchControlElement = document.getElementById("esp_status_pitch_control");
    let yawControlElement = document.getElementById("esp_status_yaw_control");
    let espConnectionStatusElement = document.getElementById("esp_connection_status");
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            const resJSON = JSON.parse(this.responseText);
            updateBadgeClasses(resJSON, pitchElement, yawElement, masterElement, pitchControlElement, yawControlElement, espConnectionStatusElement);
            setESPStatusValues(resJSON);
            updateThreeJs(resJSON);
        } else if (this.readyState === 4) {
            replace_class(espConnectionStatusElement, "badge-success", "badge-danger");
            updateInnerHTML(espConnectionStatusElement, connectionDict[false]);
        }
    };
    xhttp.open("GET", "/laser/status", true);
    xhttp.send();
}

// Update Slider Values
function updateControlValues(statusControlId, labelId, value) {
    updateInnerHTML(labelId, value);
    let controlElement = document.getElementById(statusControlId);
    swapClassOfElementOnCondition(value === controlElement.innerHTML, controlElement, "badge-warning", "badge-success");
}

function swapClassOfElementOnCondition(condition, element, swap_1, swap_2) {
    if (condition) {
        replace_class(element, swap_1, swap_2);
    } else {
        replace_class(element, swap_2, swap_1);
    }
}

let connectionDict = {
    true: "Connected",
    false: "Disconnected"
};

function setESPStatusValues(resJSON) {
    updateInnerHTML("esp_status_pitch", resJSON.pitch);
    updateInnerHTML("esp_status_pitch_control", resJSON.pitch_control);
    updateInnerHTML("esp_status_yaw", resJSON.yaw);
    updateInnerHTML("esp_status_yaw_control", resJSON.yaw_control);
    updateInnerHTML("esp_status_master", connectionDict[resJSON.reaching_master]);
    updateInnerHTML("esp_connection_status", connectionDict[resJSON.reachable_by_master]);
}

// post controls as json to the server
function postControlValues() {
    const controls = {
        pitch: parseInt(document.getElementById("pitch_range").value),
        yaw: parseInt(document.getElementById("yaw_range").value),
    };
    let xhttp = new XMLHttpRequest();
    xhttp.open("POST", "/laser/controls", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(controls));
}

// post laser control mode as json to server
function postControlMode() {
    const controls = {
        control_mode: document.getElementById("laser_mode_switch").checked ? "MANUAL" : "MICROPHONE"
    };
    let xhttp = new XMLHttpRequest();
    xhttp.open("POST", "/laser/control_mode", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(controls));
}

// add event listeners to html elements
function setupEventListeners() {
    let range_pitch = document.getElementById('pitch_range');
    if (range_pitch) {
        range_pitch.addEventListener('change', postControlValues);
        range_pitch.addEventListener('input', function () {
            updateControlValues('esp_status_pitch_control', 'pitch_value_label', range_pitch.value);
        });
    }

    let range_yaw = document.getElementById('yaw_range');
    if (range_yaw) {
        range_yaw.addEventListener('change', postControlValues);
        range_yaw.addEventListener('input', function () {
            updateControlValues('esp_status_yaw_control', 'yaw_value_label', range_yaw.value);
        });
    }

    let auto_refresh_range = document.getElementById('auto_refresh_range');
    if (auto_refresh_range) {
        auto_refresh_range.addEventListener('change', startESPStatusRefreshInterval);
        auto_refresh_range.addEventListener('input', function () {
            updateInnerHTML('auto_refresh_range_label', this.value);
        });
    }

    let refresh_esp_status = document.getElementById('refresh_esp_status');
    if (refresh_esp_status) {
        refresh_esp_status.addEventListener('click', startESPStatusRefreshInterval);
    }

    let laser_mode_switch = document.getElementById('laser_mode_switch');
    if (laser_mode_switch) {
        laser_mode_switch.addEventListener('change', postControlMode);
    }
}
