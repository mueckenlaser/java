package de.dhbw.karlsruhe.mueckenlaser.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.REQUEST_TIMEOUT, reason = "Connection to ESP timed out")
public class RequestTimeoutException extends RuntimeException {
}
