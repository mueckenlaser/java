package de.dhbw.karlsruhe.mueckenlaser.exception;

public class MathException extends Exception {

    public MathException(String s) {
        super(s);
    }
}
