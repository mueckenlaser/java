package de.dhbw.karlsruhe.mueckenlaser.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;

@Controller
public class RedirectController {
    @RequestMapping(value = "/git", method = RequestMethod.GET)
    public void redirectToGit(HttpServletResponse httpServletResponse) {
        httpServletResponse.setHeader("Location", "https://gitlab.com/mueckenlaser/java");
        httpServletResponse.setStatus(302);
    }
}
