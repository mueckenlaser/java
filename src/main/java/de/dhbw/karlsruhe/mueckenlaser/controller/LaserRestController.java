package de.dhbw.karlsruhe.mueckenlaser.controller;

import de.dhbw.karlsruhe.mueckenlaser.model.EspStatus;
import de.dhbw.karlsruhe.mueckenlaser.model.FrequencyAmplitude;
import de.dhbw.karlsruhe.mueckenlaser.model.LaserControl;
import de.dhbw.karlsruhe.mueckenlaser.model.LaserControlMode;
import de.dhbw.karlsruhe.mueckenlaser.service.EspService;
import de.dhbw.karlsruhe.mueckenlaser.service.IMicrophoneService;
import de.dhbw.karlsruhe.mueckenlaser.service.LaserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class LaserRestController {

    @Autowired
    private LaserService laserService;

    @Autowired
    private EspService espService;

    @RequestMapping(value = {"/laser/controls"}, method = RequestMethod.GET)
    @ResponseBody
    public LaserControl getControls() {
        return laserService.getLaserControl();
    }

    @RequestMapping(value = {"/laser/controls"}, method = RequestMethod.POST)
    public void setControls(@RequestBody LaserControl laserControl) {
        laserService.setControls(laserControl);
    }

    @RequestMapping(value = {"/laser/control_mode"}, method = RequestMethod.GET)
    @ResponseBody
    public LaserControlMode getControlMode() {
        return laserService.getControlMode();
    }

    @RequestMapping(value = {"/laser/control_mode"}, method = RequestMethod.POST)
    public void setControlMode(@RequestBody LaserControlMode laserControlMode) {
        laserService.setLaserControlMode(laserControlMode);
    }

    @RequestMapping(value= {"/laser/status"}, method = RequestMethod.GET)
    @ResponseBody
    public EspStatus getLaserStatus() {
        return espService.getStatus();
    }

    @RequestMapping(value = {"/status"}, method = RequestMethod.GET)
    @ResponseBody
    public LaserControl getDummyStatus() {
        return laserService.getLaserControl();
    }

    @RequestMapping(value = {"/laser/controls/point_to"}, method = RequestMethod.POST)
    @ResponseBody
    public void point(@RequestParam double x, @RequestParam double y, @RequestParam double z) {
        LaserControl laserControl = LaserService.calculatePitchAndYaw(x, y, z);
        laserService.setControls(laserControl);
    }
}
