package de.dhbw.karlsruhe.mueckenlaser.controller;

import de.dhbw.karlsruhe.mueckenlaser.model.FrequencyAmplitude;
import de.dhbw.karlsruhe.mueckenlaser.service.IMicrophoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MicrophoneRestController {

    @Autowired
    private IMicrophoneService microphoneService;

    @RequestMapping(value = {"/microphone/{microphone}/amplitudes"}, method = RequestMethod.GET)
    @ResponseBody
    public List<FrequencyAmplitude> microphone_data(@PathVariable int microphone) {
        return microphoneService.getCurrentFrequencySpectrum(microphone);
    }

    @RequestMapping(value = {"/microphone/{microphone}/noise-intensity"}, method = RequestMethod.GET)
    @ResponseBody
    public double noiseIntensity(@PathVariable int microphone) {
        return microphoneService.getNoiseIntensity(microphone);
    }
}
