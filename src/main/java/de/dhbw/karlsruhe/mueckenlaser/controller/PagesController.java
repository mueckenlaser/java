package de.dhbw.karlsruhe.mueckenlaser.controller;

import de.dhbw.karlsruhe.mueckenlaser.service.LaserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class PagesController {

    @Autowired
    private LaserService laserService;

    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String index() {
        return "index";
    }

    @RequestMapping(value = {"/esp_dummy"}, method = RequestMethod.GET)
    public String espDummy() {
        return "esp_dummy";
    }

    @RequestMapping(value = {"/laser"}, method = RequestMethod.GET)
    public String laser(Model model) {
        model.addAttribute("laser_control_mode_checked", laserService.getControlMode().toString().equals("MANUAL"));
        return "laser";
    }

    @RequestMapping(value = {"/microphone"}, method = RequestMethod.GET)
    public String microphoneGraph() {
        return "microphone_graph";
    }
}
