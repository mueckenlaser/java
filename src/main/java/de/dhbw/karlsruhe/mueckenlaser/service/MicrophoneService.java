package de.dhbw.karlsruhe.mueckenlaser.service;

import de.dhbw.karlsruhe.mueckenlaser.audioprocessing.MicrophoneInputThread;
import de.dhbw.karlsruhe.mueckenlaser.model.FrequencyAmplitude;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * The MicrophoneService provides data produced by the connected microphones.
 */
@Service
public class MicrophoneService implements IMicrophoneService {
    // specify the names of the microphones used by the operating system (can be changed on Windows 10)
    @Value("${base.microphone-names}")
    String[] microphoneNames;

    private MicrophoneInputThread microphoneInputThread;

    @Autowired
    private LaserService laserService;

    @PostConstruct
    public void init() {
        this.microphoneInputThread = new MicrophoneInputThread(microphoneNames, laserService);
        this.microphoneInputThread.start();
    }

    @Override
    public List<FrequencyAmplitude> getCurrentFrequencySpectrum(int id) {
        return microphoneInputThread.getCurrentFrequencySpectrum(id);
    }

    @Override
    public double getNoiseIntensity(int id) {
        return microphoneInputThread.getNoiseIntensity(id);
    }

}
