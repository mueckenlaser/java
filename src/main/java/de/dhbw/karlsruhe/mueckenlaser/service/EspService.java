package de.dhbw.karlsruhe.mueckenlaser.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.dhbw.karlsruhe.mueckenlaser.monitoring.EspStatusThread;
import de.dhbw.karlsruhe.mueckenlaser.model.EspStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.time.Duration;
import java.util.Random;

@Service
public class EspService {
    Duration timeout = Duration.ofMillis(500);
    RestTemplate restTemplate = new RestTemplateBuilder().setConnectTimeout(timeout).setReadTimeout(timeout).build();
    Random random = new Random();
    EspStatusThread thread = new EspStatusThread(this);

    @Value("${esp.root_url}")
    private String LASER_URL;
    private EspStatus espStatus = new EspStatus();

    @Autowired
    ObjectMapper objectMapper;

    @PostConstruct
    public void init() {
        thread.start();
    }

    public EspStatus getDummyStatus() {
        return new EspStatus(espStatus.getPitch(), espStatus.getPitchControl(), espStatus.getYaw(), espStatus.getYawControl(), true, true);
    }

    public EspStatus requestStatusJsonFromEsp() throws ResourceAccessException {
        final String uri = LASER_URL + "/status";
        String request;

        try {
            request = restTemplate.getForObject(uri, String.class);
        } catch (HttpClientErrorException ignored) {
            System.out.println("404 from ESP");
            return new EspStatus();
        }

        try {
            return objectMapper.readValue(request, EspStatus.class);
        } catch (IOException e) {
            // This should not happen
            e.printStackTrace();
            return new EspStatus();
        }
    }

    public EspStatus getStatus() {
        return espStatus;
    }

    public void setEspStatus(EspStatus espStatus) {
        this.espStatus = espStatus;
    }
}
