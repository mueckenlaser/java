package de.dhbw.karlsruhe.mueckenlaser.service;

import de.dhbw.karlsruhe.mueckenlaser.model.FrequencyAmplitude;

import java.util.List;

/**
 * The IMicrophoneService Interface defines the methods the MicrophoneService needs to implement to provide the data of
 * the connected microphones.
 */
public interface IMicrophoneService {

    /**
     * Provides a list of tuples containing the current amplitudes at specific frequencies
     *
     * @param id The internal ID of the microphone. possible value range with 3 microphones: 0 - 2
     * @return A list of FrequencyAmplitude objects containing the current amplitudes
     */
    public List<FrequencyAmplitude> getCurrentFrequencySpectrum(int id);

    /**
     * Calculates the current noise intensity with the active noise pattern at the specified microphone
     *
     * @param id The internal ID of the microphone. possible value range with 3 microphones: 0 - 2
     * @return The noise intensity at the specified microphone
     */
    public double getNoiseIntensity(int id);

}
