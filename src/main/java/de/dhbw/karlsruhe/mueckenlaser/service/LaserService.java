package de.dhbw.karlsruhe.mueckenlaser.service;

import de.dhbw.karlsruhe.mueckenlaser.model.LaserControl;
import de.dhbw.karlsruhe.mueckenlaser.model.LaserControlMode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;

/**
 * Service for controlling the laser turret
 */
@Service
public class LaserService {
    /**
     * Laser X coordinate
     */
    private static final int LASER_X = 0;
    /**
     * Laser Y coordinate
     */
    private static final int LASER_Y = 0;
    /**
     * Laser Z coordinate
     */
    private static final int LASER_Z = 0;

    @Value("${laser.default_control_mode}")
    private String DEFAULT_LASER_CONTROL_MODE;

    private LaserControl laserControl = new LaserControl(0, 0);
    private LaserControlMode laserControlMode;

    @PostConstruct
    public void init() {
        this.laserControlMode = new LaserControlMode(DEFAULT_LASER_CONTROL_MODE);
    }

    /**
     * Calculates pitch and yaw to point the laser at a specific coordinate.
     *
     * @return HashMap containing pitch and yaw
     */
    public static LaserControl calculatePitchAndYaw(double x, double y, double z) {
        // Difference between laser X and given X coordinate
        final double DX = x - LASER_X;
        // Difference between laser Y and given Y coordinate
        final double DY = y - LASER_Y;
        // Difference between laser Z and given Z coordinate
        final double DZ = z - LASER_Z;

        // Formulas found on Stackexchange
        // https://math.stackexchange.com/questions/470112/calculate-camera-pitch-yaw-to-face-point
        int pitch = (int) Math.round(Math.toDegrees(Math.atan2(DZ, Math.sqrt(DX * DX + DY * DY)))) + 90;
        int yaw = (int) Math.round(Math.toDegrees(Math.atan2(DY, DX)));
        if (yaw < 0) {
            yaw = 360 + yaw;
        }

        return new LaserControl(pitch, yaw);
    }

    /**
     * Getter for LaserControl JSONObject
     *
     * @return JSONObject
     */
    public LaserControl getLaserControl() {
        return laserControl;
    }

    /**
     * Setter for LaserControl
     */
    public void setControls(LaserControl laserControl) {
        this.laserControl = laserControl;
    }

    /**
     * Getter for LaserControlMode JSONObject
     * <p>
     * return JSONObject
     */
    public LaserControlMode getControlMode() {
        return laserControlMode;
    }

    /**
     * Setter for LaserControlMode
     */
    public void setLaserControlMode(LaserControlMode laserControlMode) {
        this.laserControlMode = laserControlMode;
    }
}
