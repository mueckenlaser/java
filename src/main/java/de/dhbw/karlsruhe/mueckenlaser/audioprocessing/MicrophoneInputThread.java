package de.dhbw.karlsruhe.mueckenlaser.audioprocessing;

import de.dhbw.karlsruhe.mueckenlaser.exception.LocationOfNoiseNotFoundException;
import de.dhbw.karlsruhe.mueckenlaser.localization.FlyLocalisation;
import de.dhbw.karlsruhe.mueckenlaser.localization.math.Vector;
import de.dhbw.karlsruhe.mueckenlaser.model.FrequencyAmplitude;
import de.dhbw.karlsruhe.mueckenlaser.model.LaserControl;
import de.dhbw.karlsruhe.mueckenlaser.model.LaserControlMode;
import de.dhbw.karlsruhe.mueckenlaser.model.Microphone;
import de.dhbw.karlsruhe.mueckenlaser.service.LaserService;
import de.dhbw.karlsruhe.mueckenlaser.audioprocessing.soundfilter.IdealFlyPattern;
import de.dhbw.karlsruhe.mueckenlaser.audioprocessing.soundfilter.INoisePattern;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Line;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.TargetDataLine;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * The MicrophoneInputThread continuously calculates the Fourier Transform for the connected microphones.
 */
public class MicrophoneInputThread extends Thread {

    // the number of amplitude arrays to be saved in the buffer
    private final int bufferSize = 8;
    private INoisePattern noisePattern;
    // names of the microphones
    private final String[] microphoneNames;
    // the TargetDataLines to listen to
    private List<TargetDataLine> targetDataLines;
    // the sample rate to be used by the TargetDataLine
    private int sampleRate = 48000;
    // the size of the blocks to be used by the Fourier Transform
    // higher for better frequencyResolution, lower for faster updates
    private int blockLength = 1024;
    // distance between two frequencies in Hz
    private float frequencyResolution = (float) sampleRate / blockLength;
    // the buffer to save the last amplitudes in
    private List<LinkedList<float[]>> amplitudeBuffers = new ArrayList<>();
    private List<Microphone> microphones = new ArrayList<>();

    private LaserControlMode previousLaserControlMode;

    private LaserService laserService;

    private int counter;

    public MicrophoneInputThread(String[] microphoneNames, LaserService laserService) {
        // the default noise pattern is FlyPattern
        this.noisePattern = new IdealFlyPattern();
        this.microphoneNames = microphoneNames;
        this.laserService = laserService;

        previousLaserControlMode = laserService.getControlMode();
        Map<String, TargetDataLine> targetDataLineMap = getTargetDataLines(microphoneNames);
        this.targetDataLines = new ArrayList<>();
        for (String name : microphoneNames) {
            this.targetDataLines.add(targetDataLineMap.get(name));
            // add buffer for amplitudes for each TargetDataLine
            this.amplitudeBuffers.add(new LinkedList<>());
        }

        // Create 3 empty microphones
        for (int i = 0; i < 3; i++) {
            this.microphones.add(new Microphone());
        }
    }

    @Override
    public void run() {
        // the Encoding of the AudioFormat for the TargetDataLine
        AudioFormat.Encoding encoding = AudioFormat.Encoding.PCM_SIGNED;

        // the number of bits in each sample
        int sampleSizeInBits = 16;

        // the number of audio channels (1 for mono)
        int channels = 1;

        // the size of frames in bytes
        int frameSize = 2;

        // how the data of samples is stored (false for littleEndian)
        boolean bigEndian = false;

        // open and start the TargetDataLine with the specified format
        AudioFormat format = new AudioFormat(encoding, sampleRate, sampleSizeInBits, channels, frameSize, sampleRate, bigEndian);

        List<AudioInputStream> audioInputStreams = new ArrayList<>();

        // start an AudioInputStream from the TargetDataLine to be able to access the data
        for (int i = 0; i < targetDataLines.size(); i++) {
            TargetDataLine targetDataLine = targetDataLines.get(i);
            if (targetDataLine == null) {
                System.out.println(this.microphoneNames[i] + ": not connected");
            }
            if (targetDataLine != null) {
                try {
                    targetDataLine.open(format, blockLength);
                    targetDataLine.start();
                    audioInputStreams.add(new AudioInputStream(targetDataLine));
                } catch (LineUnavailableException e) {
                    e.printStackTrace();
                }
            }
        }

        while (true) {
            if (laserService.getControlMode().equals(new LaserControlMode("MANUAL"))) {
                // clear amplitude buffer of microphones if mode has just been toggled
                if (previousLaserControlMode.equals(new LaserControlMode("MICROPHONE"))) {
                    for (List amplitudeBuffer : this.amplitudeBuffers) {
                        amplitudeBuffer.clear();
                    }
                }
                this.previousLaserControlMode = new LaserControlMode("MANUAL");

                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt(); // Here!
                    throw new RuntimeException(e);
                }
                continue;
            }
            this.previousLaserControlMode = new LaserControlMode("MICROPHONE");

            List<FourierTransformThread> threads = new ArrayList<>();
            for (AudioInputStream audioInputStream : audioInputStreams) {
                FourierTransformThread t = new FourierTransformThread(audioInputStream, blockLength, frameSize);
                t.start();
                threads.add(t);
            }

            waitForThreadsToFinish(threads);

            for (int i = 0; i < threads.size(); i++) {
                // add the amplitudes to the ring buffer
                float[] amplitudes = threads.get(i).getAmplitudes();
                this.amplitudeBuffers.get(i).add(amplitudes);

                // if the ring buffer is full, remove the oldest entry
                if (amplitudeBuffers.get(i).size() > this.bufferSize) {
                    this.amplitudeBuffers.get(i).removeFirst();
                }

                // calculate frequency spectrum and noise intensity
                calculateCurrentFrequencySpectrum(i);
                calculateNoiseIntensity(i);
            }

            // calculate the position of the fly
            double noiseIntensity0 = getNoiseIntensity(0);
            double noiseIntensity1 = getNoiseIntensity(1);
            double noiseIntensity2 = getNoiseIntensity(2);
            Vector position;
            try {
                position = FlyLocalisation.calculateVectorPointingToNoise(noiseIntensity0, noiseIntensity1, noiseIntensity2);
                if (counter == 20) {
                    System.out.println(position.toString()); //TODO Remove this commit again
                    counter = 0;
                } else {
                    counter += 1;
                }


                // send the position to the laserService that provides it for the ESP controlling the laser
                LaserControl laserControl = LaserService.calculatePitchAndYaw(position.getX(), position.getY(), position.getZ());
                laserService.setControls(laserControl);
            } catch (LocationOfNoiseNotFoundException e) {// don't update laserControl if position could not be found
            }
        }

    }

    /**
     * Returns, when all the threads are finished.
     *
     * @param threads the threads the method should wait for to finish
     */
    private void waitForThreadsToFinish(List<FourierTransformThread> threads) {
        boolean done = false;
        while (!done) {
            done = true;
            for (FourierTransformThread t : threads) {
                if (t.isAlive()) {
                    done = false;
                }
            }
        }
    }

    private void calculateCurrentFrequencySpectrum(int id) {
        Microphone m = this.microphones.get(id);
        // return an empty list when microphone is not connected
        if (this.amplitudeBuffers.get(id).isEmpty()) {
            m.setFrequencyAmplitudes(new ArrayList<>());
            return;
        }

        // initialize a new empty array to later save the average amplitude for each frequency
        // the Fourier Transform calculates exactly half as many frequencies as samples it gets (blockLength)
        float[] averageAmplitudes = new float[blockLength / 2];

        // iterate over the buffer and add the amplitudes for all frequencies together
        for (float[] entry : amplitudeBuffers.get(id)) {
            // iterate over the list of amplitudes
            for (int i = 0; i < averageAmplitudes.length; i++) {
                // amplitude can be positive and negative, we only want absolute values
                averageAmplitudes[i] += Math.abs(entry[i]) / bufferSize;
            }
        }

        // create a list that contains the frequencies and their corresponding average amplitude
        List<FrequencyAmplitude> frequencyAmplitudes = new ArrayList<>();
        for (int i = 0; i < averageAmplitudes.length; i++) {
            frequencyAmplitudes.add(new FrequencyAmplitude((i + 1) * frequencyResolution, averageAmplitudes[i]));
        }
        m.setFrequencyAmplitudes(frequencyAmplitudes);
    }

    /**
     * Returns the average amplitudes of the buffered FrequencyAmplitude lists
     *
     * @param id id of the microphone
     * @return A list of FrequencyAmplitude objects
     */
    public List<FrequencyAmplitude> getCurrentFrequencySpectrum(int id) {
        Microphone m = this.microphones.get(id);
        return m.getFrequencyAmplitudes();
    }

    /**
     * Calculates the current noise intensity with the active noise pattern at the specified microphone
     *
     * @param id The internal ID of the microphone. possible value range with 3 microphones: 0 - 2
     */
    private void calculateNoiseIntensity(int id) {
        Microphone m = this.microphones.get(id);
        List<FrequencyAmplitude> frequencyAmplitudes = getCurrentFrequencySpectrum(id);
        // return 0 when microphone is not connected
        if (frequencyAmplitudes.isEmpty()) {
            m.setNoiseIntensity(0);
            return;
        }
        m.setNoiseIntensity(noisePattern.getIntensity(frequencyAmplitudes));
    }

    /**
     * Returns the current noise intensity with the active noise pattern at the specified microphone
     *
     * @param id The internal ID of the microphone. possible value range with 3 microphones: 0 - 2
     * @return The noise intensity at the specified microphone
     */
    public double getNoiseIntensity(int id) {
        Microphone m = this.microphones.get(id);
        return m.getNoiseIntensity();
    }


    /**
     * Searches for the TargetDataLines of the microphones with the specified names. (You can specify the
     * name of your microphones on Windows 10 on Settings > System > Sound)
     *
     * @param names The names of the microphones on the operating system
     * @return A Map of the TargetDataLines of the specified microphone names. The names are used as key for the map
     */
    private Map<String, TargetDataLine> getTargetDataLines(String[] names) {
        // create an empty HashMap to later store the TargetDataLines in it
        Map<String, TargetDataLine> targetDataLineMap = new HashMap<>();

        StringBuilder connectedMics = new StringBuilder("Connected audio devices: ");

        // iterate over all installed audio mixers
        Mixer.Info[] mixerInfos = AudioSystem.getMixerInfo();
        for (Mixer.Info info : mixerInfos) {
            Mixer m = AudioSystem.getMixer(info);
            Line.Info[] lineInfos = m.getTargetLineInfo();

            // only look for TargetDataLines
            if (lineInfos.length >= 1 && lineInfos[0].getLineClass().equals(TargetDataLine.class)) {

                // iterate over all TargetDataLines in the mixer
                for (Line.Info lineInfo : lineInfos) {
                    Line line = null;
                    try {
                        line = m.getLine(lineInfo);
                    } catch (LineUnavailableException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    connectedMics.append(info.getName()).append(", ");

                    // check if the name of the TargetDataLine matches one name in the names list
                    for (String name : names) {

                        // contains() instead of equals(), because you can only set the first part of the name (on Windows)
                        if (info.getName().contains(name)) {
                            targetDataLineMap.put(name, (TargetDataLine) line);
                            System.out.println(name + ": " + line);
                        }
                    }
                }
            }
        }

        // Remove last two chars from String (", ")
        connectedMics.setLength(connectedMics.length() - 2);
        System.out.println(connectedMics);

        return targetDataLineMap;
    }
}
