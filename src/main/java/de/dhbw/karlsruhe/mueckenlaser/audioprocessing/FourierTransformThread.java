package de.dhbw.karlsruhe.mueckenlaser.audioprocessing;

import org.jtransforms.fft.FloatFFT_1D;

import javax.sound.sampled.AudioInputStream;
import java.nio.ByteBuffer;

public class FourierTransformThread extends Thread {

    // create an array to hold the calculated amplitudes
    // the Fourier Transform calculates exactly half as many frequencies as samples it gets (blockLength)
    private float[] amplitudes;

    private AudioInputStream audioInputStream;
    private int frameSize;
    private int blockLength;

    public FourierTransformThread(AudioInputStream audioInputStream, int blockLength, int frameSize) {
        this.audioInputStream = audioInputStream;
        this.blockLength = blockLength;
        this.frameSize = frameSize;
    }

    public void run() {
        amplitudes = new float[blockLength / 2];
        // create a buffer to hold the bytes collected from the AudioInputStream
        byte[] buffer = new byte[blockLength * frameSize];
        // create an array to hold the values read from the byte buffer
        float[] values = new float[blockLength];

        try {
            // read the most recent bytes from the AudioInputStream
            audioInputStream.read(buffer);
            ByteBuffer bb = ByteBuffer.wrap(buffer);

            // get the float values from the byte buffer
            for (int i = 0; i < buffer.length / frameSize; i++) {
                values[i] = bb.getShort();
            }

            // execute the Fast Fourier Transform on the read values
            // the result is stored in the values array
            FloatFFT_1D fft = new FloatFFT_1D(blockLength);
            fft.realForward(values);
            // According to the JTransform documentation the result of realForward() for even n is saved as follows
            // a[2*k] = Re[k], 0<=k<n/2
            // a[2*k+1] = Im[k], 0<k<n/2
            // a[1] = Re[n/2]
            // Re[k] are the amplitudes of the different frequencies
            // Im[k] are the phases of the different frequencies

            // write the amplitudes in a separate array
            // every second value is the amplitude of a frequency
            for (int i = 0; i < amplitudes.length; i++) {
                amplitudes[i] = values[2 * i];
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public float[] getAmplitudes() {
        return amplitudes;
    }
}
