package de.dhbw.karlsruhe.mueckenlaser.audioprocessing.soundfilter;

import de.dhbw.karlsruhe.mueckenlaser.model.FrequencyAmplitude;

import java.util.List;

/**
 * The INoisePattern Interface defines the methods a NoisePattern needs to implement.
 */
public interface INoisePattern {

    /**
     * Calculates the intensity of the noise from a list of FrequencyAmplitude objects
     *
     * @param frequencyAmplitudes The list of FrequencyAmplitude objects
     * @return The intensity of the noise
     */
    public double getIntensity(List<FrequencyAmplitude> frequencyAmplitudes);

}
