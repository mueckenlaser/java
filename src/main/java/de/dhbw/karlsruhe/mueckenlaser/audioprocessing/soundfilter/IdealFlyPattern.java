package de.dhbw.karlsruhe.mueckenlaser.audioprocessing.soundfilter;

import de.dhbw.karlsruhe.mueckenlaser.model.FrequencyAmplitude;

import java.util.List;

/**
 * This Pattern describes an ideal fly that only makes noise at specified frequency/frequencies. For each entry only two
 * values are used for the noise intensity: The next higher and the next lower frequency calculated by the Fourier
 * Transform. By default the flyPattern only contains the frequency 1200Hz.
 */
public class IdealFlyPattern implements INoisePattern {

    /**
     * list of frequencies to be used in pattern and their weighting
     */
    private double[][] flyPattern;

    /**
     * Constructor of FlyPattern. Use default pattern with 1200Hz
     */
    public IdealFlyPattern() {
        this(new double[][]{{600, 1.0}});
    }

    /**
     * Constructor of FlyPattern.
     *
     * @param flyPattern array of the frequencies to be used and their weighting. Example to only use the frequency
     *                   1200Hz at a weighting of 1: {{1200, 1.0}}
     */
    public IdealFlyPattern(double[][] flyPattern) {
        this.flyPattern = flyPattern;
    }

    /**
     * Calculates the intensity of the noise described by the FlyPattern from a list of FrequencyAmplitude objects
     *
     * @param frequencyAmplitudes The list of FrequencyAmplitude objects
     * @return intensity of the noise
     */
    @Override
    public double getIntensity(List<FrequencyAmplitude> frequencyAmplitudes) {
        double intensity = 0;
        float faDistance = frequencyAmplitudes.get(1).frequency;
        int i = 0;
        for (FrequencyAmplitude fa : frequencyAmplitudes) {
            // add the value for the frequency right below the defined pattern
            if (fa.frequency <= flyPattern[i][0] && fa.frequency + faDistance >= flyPattern[i][0]) {
                intensity += fa.amplitude * flyPattern[i][1];
            }
            // add the value for the frequency right above the defined pattern
            if (fa.frequency >= flyPattern[i][0] && fa.frequency - faDistance <= flyPattern[i][0]) {
                intensity += fa.amplitude * flyPattern[i][1];
                i++;
            }
            if (i >= flyPattern.length)
                break;
        }
        return intensity;
    }
}
