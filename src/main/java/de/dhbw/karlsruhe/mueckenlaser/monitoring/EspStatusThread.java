package de.dhbw.karlsruhe.mueckenlaser.monitoring;

import de.dhbw.karlsruhe.mueckenlaser.model.EspStatus;
import de.dhbw.karlsruhe.mueckenlaser.service.EspService;
import org.springframework.web.client.ResourceAccessException;

import java.util.concurrent.TimeUnit;

/**
 * The EspStatusThread regularly calls the ESP and saves its status
 */
public class EspStatusThread extends Thread {
    private final EspService espService;

    public EspStatusThread(EspService espService) {
        this.espService = espService;
    }

    public void run() {
        while (true) {
            try {
                espService.setEspStatus(espService.requestStatusJsonFromEsp());
            } catch (ResourceAccessException e) {
                // (ResourceAccessException) ESP is not responding therefore unreachable
                espService.setEspStatus(new EspStatus()); // EspStatus defaults to unreachable by master so this is fine here.
            }

            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                // Restore the interrupted status
                Thread.currentThread().interrupt();
            }
        }
    }

}
