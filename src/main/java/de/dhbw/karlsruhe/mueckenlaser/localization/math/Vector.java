package de.dhbw.karlsruhe.mueckenlaser.localization.math;

public class Vector {
    private final double x, y, z;

    public Vector(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public static Vector getDirectionVector(Vector from, Vector to) {
        double x = to.x - from.x;
        double y = to.y - from.y;
        double z = to.z - from.z;
        return new Vector(x, y, z);
    }

    public static Vector getCrossProduct(Vector a, Vector b) {
        double x = a.y * b.z - a.z * b.y;
        double y = a.z * b.x - a.x * b.z;
        double z = a.x * b.y - a.y * b.x;
        return new Vector(x, y, z);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Vector) {
            Vector v = (Vector) obj;
            // allow minor deviations
            boolean xEqual = Math.abs(this.x - v.x) < 1e-10;
            boolean yEqual = Math.abs(this.y - v.y) < 1e-10;
            boolean zEqual = Math.abs(this.z - v.z) < 1e-10;
            return xEqual && yEqual && zEqual;
        }
        return true;
    }

    /**
     * checks if two vectors are codirectional
     *
     * @param v vector to check if it is codirectional
     * @return true if vectors are codirectional
     */
    public boolean isCodirectional(Vector v) {
        // if a * b = ||a|| * ||b|| vectors are codirectional
        return Math.abs(this.dotProduct(v) - (this.getVectorLength() * v.getVectorLength())) < 1e-10; // allow minor deviations
    }

    private double dotProduct(Vector v) {
        return this.x * v.x + this.y * v.y + this.z * v.z;
    }

    /**
     * Calculates the length of the vector
     *
     * @return length of the vector
     */
    private double getVectorLength() {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2));
    }

    @Override
    public String toString() {
        return "Vector(" + this.x + ", " + this.y + ", " + this.z + ")";
    }
}
