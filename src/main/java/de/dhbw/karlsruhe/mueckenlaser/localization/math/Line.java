package de.dhbw.karlsruhe.mueckenlaser.localization.math;

public class Line {
    private final Vector origin;
    private final Vector direction;

    public Line(Vector origin, Vector direction) {
        this.origin = origin;
        this.direction = direction;
    }

    public Vector getOrigin() {
        return origin;
    }

    public Vector getDirection() {
        return direction;
    }
}
