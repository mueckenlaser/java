package de.dhbw.karlsruhe.mueckenlaser.localization.math;

import Jama.Matrix;
import de.dhbw.karlsruhe.mueckenlaser.exception.MathException;

/**
 * Representation of a plane in 3 dimensional space in the form:<br>
 * aX + bY + c*Z + d = 0
 */
public class Plane {
    private final double a, b, c, d;

    public Plane(double a, double b, double c, double d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    public static Plane getIntersectingPlaneBetweenSpheres(Sphere s0, Sphere s1) throws MathException {
        if (s0.getCenter().equals(s1.getCenter())) {
            throw new MathException("Cannot create intersecting plane, because spheres have the same center");
        }
        if (!s0.isCollidingWith(s1)) {
            throw new MathException("Cannot create intersecting plane, because spheres dont intersect");
        }

        double a = s0.getA() - s1.getA();
        double b = s0.getB() - s1.getB();
        double c = s0.getC() - s1.getC();
        double d = s0.getD() - s1.getD();
        return new Plane(a, b, c, d);
    }

    public static Vector getIntersectingPointOfPlanes(Plane p0, Plane p1, Plane p2) throws MathException {
        boolean p0p1Codirectional = p0.getNormalVector().isCodirectional(p1.getNormalVector());
        boolean p0p2Codirectional = p0.getNormalVector().isCodirectional(p2.getNormalVector());
        boolean p1p2Codirectional = p1.getNormalVector().isCodirectional(p2.getNormalVector());
        if (p0p1Codirectional || p0p2Codirectional || p1p2Codirectional) {
            throw new MathException("The planes don't have a intersection point in common because two planes are parallel");
        }

        // create arrays representing the plane equations as matrix (Ax=b)
        double[][] AArray = {{p0.a, p0.b, p0.c}, {p1.a, p1.b, p1.c}, {p2.a, p2.b, p2.c}};
        double[] bArray = {-p0.d, -p1.d, -p2.d};

        // create matrix from arrays
        Matrix A = new Matrix(AArray);
        Matrix b = new Matrix(bArray, 3);

        // solve matrix
        Matrix x = A.solve(b);

        // create Vector from the result
        Vector intersectingPoint = new Vector(x.get(0, 0), x.get(1, 0), x.get(2, 0));

        // check if Point really is in all planes
        assert Plane.isPointInPlane(intersectingPoint, p0);
        assert Plane.isPointInPlane(intersectingPoint, p1);
        assert Plane.isPointInPlane(intersectingPoint, p2);
        return intersectingPoint;
    }

    public static boolean isPointInPlane(Vector point, Plane plane) {
        // insert the point into the plane equation is true
        return Math.abs(point.getX() * plane.a + point.getY() * plane.b + point.getZ() * plane.c + plane.d) < 1e-10; // allow minor deviations
    }

    public static Plane getPlaneFromPoints(Vector p0, Vector p1, Vector p2) throws MathException {
        // vector from p0 to p1
        Vector v01 = Vector.getDirectionVector(p0, p1);
        // vector from p0 to p2
        Vector v02 = Vector.getDirectionVector(p0, p2);

        if (v01.isCodirectional(v02)) {
            throw new MathException("Cannot create Plane because all three points are on one line");
        }

        // calculate the normal vector of the two direction vectors of the plane
        Vector normal = Vector.getCrossProduct(v01, v02);
        double a = normal.getX();
        double b = normal.getY();
        double c = normal.getZ();

        // solve the linear equation to get d:
        // aX + bY + c*Z + d = 0
        // d = - a*x0 - b*y0 - c*z0
        double d = -a * p0.getX() - b * p0.getY() - c * p0.getZ();

        // check if the points are in the plane
        assert (a * p0.getX() + b * p0.getY() + c * p0.getZ() + d) == 0;
        assert (a * p1.getX() + b * p1.getY() + c * p1.getZ() + d) == 0;
        assert (a * p2.getX() + b * p2.getY() + c * p2.getZ() + d) == 0;
        return new Plane(a, b, c, d);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Plane) {
            Plane p = (Plane) obj;
            if (this.a == p.a && this.b == p.b && this.c == p.c && this.d == p.d) {
                return true;
            }

            // check if the other plane is a multiple of this one
            if (this.a != 0 && this.a != p.a) {
                double factor = this.a / p.a;
                return this.b == factor * p.b && this.c == factor * p.c && this.d == factor * p.d;
            }
            if (this.b != 0 && this.b != p.b) {
                double factor = this.b / p.b;
                return this.c == factor * p.c && this.d == factor * p.d;
            }
            if (this.c != 0 && this.c != p.c) {
                double factor = this.c / p.c;
                return this.d == factor * p.d;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Plane: " + this.a + "x + " + this.b + "y + " + this.c + "z + " + this.d + " = 0";
    }

    public Vector getNormalVector() {
        return new Vector(this.a, this.b, this.c);
    }
}
