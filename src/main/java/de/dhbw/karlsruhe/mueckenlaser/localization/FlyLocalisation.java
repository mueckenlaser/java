package de.dhbw.karlsruhe.mueckenlaser.localization;

import de.dhbw.karlsruhe.mueckenlaser.exception.MathException;
import de.dhbw.karlsruhe.mueckenlaser.exception.LocationOfNoiseNotFoundException;
import de.dhbw.karlsruhe.mueckenlaser.localization.math.Sphere;
import de.dhbw.karlsruhe.mueckenlaser.localization.math.Vector;

public class FlyLocalisation {

    /**
     * Calculates a vector that points in the direction of the source of the noise
     *
     * @return a vector pointing in the direction of the noise
     */
    public static Vector calculateVectorPointingToNoise(double noiseIntensity0, double noiseIntensity1, double noiseIntensity2) throws LocationOfNoiseNotFoundException {
        double averageIntensity = (noiseIntensity0 + noiseIntensity1 + noiseIntensity2) / 3;

        double distance0 = estimateDistance(noiseIntensity0, averageIntensity);
        double distance1 = estimateDistance(noiseIntensity1, averageIntensity);
        double distance2 = estimateDistance(noiseIntensity2, averageIntensity);

        // equilateral triangle with a side length of 1LE
        // coordinate origin is exactly in the middle of the triangle
        Vector position0 = new Vector(0.577, 0, 0);
        Vector position1 = new Vector(-0.289, 0.5, 0);
        Vector position2 = new Vector(-0.289, -0.5, 0);

        Sphere sphere0 = new Sphere(position0, distance0);
        Sphere sphere1 = new Sphere(position1, distance1);
        Sphere sphere2 = new Sphere(position2, distance2);

        // calculate the intersecting points of the three spheres
        Vector[] intersectingPoints;
        try {
            intersectingPoints = Sphere.getIntersectingPoints(sphere0, sphere1, sphere2);
        } catch (MathException e) {
            throw new LocationOfNoiseNotFoundException();
        }

        // the three spheres have two intersecting points, of which one is below our laser and microphones
        // as our laser is supposed to stand on the floor, the fly can only be above our laser and we can ignore the
        // second possible position
        return getPositionAboveGround(intersectingPoints[0], intersectingPoints[1]);
    }

    /**
     * Estimates the distance of the noise to the microphone based on the noise intensity there and the average noise
     * intensity on all microphones.
     *
     * @param noiseIntensity the noise intensity at the microphone
     * @param averageIntensity the average noise intensity at all microphones
     * @return
     */
    private static double estimateDistance(double noiseIntensity, double averageIntensity) {
        // higher noise intensity than average means the noise source is close -> the radius should be smaller
        return averageIntensity / noiseIntensity;
    }

    /**
     * Selects the position with the higher Z-value
     *
     * @param position0 first position
     * @param position1 second position
     * @return the position with the higher Z-value
     */
    private static Vector getPositionAboveGround(Vector position0, Vector position1) {
        if (position0.getZ() < position1.getZ()) {
            return position1;
        }
        return position0;
    }
}
