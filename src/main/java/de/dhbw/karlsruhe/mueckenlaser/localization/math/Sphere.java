package de.dhbw.karlsruhe.mueckenlaser.localization.math;

import de.dhbw.karlsruhe.mueckenlaser.exception.MathException;

/**
 * Representation of a sphere in 3 dimensional space.
 * Provides center position, radius and its equation in the form:<br>
 * X^2 + Y^2 + Z^2 + aX + bY + c*Z + d = 0
 */
public class Sphere {

    private final Vector center;
    private final double radius;
    private final double a, b, c, d;

    public Sphere(Vector center, double radius) {
        this.center = center;
        this.radius = radius;

        // Calculate the equation of the sphere from its center position and radius
        // Explanation:
        // (X - s.x)^2 + (Y - s.y)^2 + (Z - s.z)^2 = s.radius^2
        // -> (X^2 - 2*s.x*X + s.x^2) + <same for Y and Z> = s.radius^2
        // -> X^2 + Y^2 + Z^2 + aX + bY + c*Z + d = 0

        // a = -2 * s.x
        this.a = -2 * center.getX();
        // b = -2 * s.y
        this.b = -2 * center.getY();
        // c = -2 * s.z
        this.c = -2 * center.getZ();
        // d = s.x^2 + s.y^2 + s.z^2 - s.radius^2
        this.d = Math.pow(center.getX(), 2)
                + Math.pow(center.getY(), 2)
                + Math.pow(center.getZ(), 2)
                - Math.pow(radius, 2);
    }

    /**
     * Calculates the two intersection points of three spheres.
     * The algorithm is based on https://stackoverflow.com/a/1406558
     *
     * @param s0 Sphere 0
     * @param s1 Sphere 1
     * @param s2 Sphere 2
     * @return the positions of the two intersection points
     */
    public static Vector[] getIntersectingPoints(Sphere s0, Sphere s1, Sphere s2) throws MathException {
        // compute the equations of P12 P23 P32 (difference of sphere equations)
        Plane p01;
        Plane p12;
        Plane p02;
        try {
            p01 = Plane.getIntersectingPlaneBetweenSpheres(s0, s1);
            p12 = Plane.getIntersectingPlaneBetweenSpheres(s1, s2);
            p02 = Plane.getIntersectingPlaneBetweenSpheres(s0, s2);
        } catch (MathException e) {
            throw new MathException("The spheres don't intersect");
        }

        // compute the equation of Q  (plane of the microphones)
        Plane q;
        try {
            q = Plane.getPlaneFromPoints(s0.getCenter(), s1.getCenter(), s2.getCenter());
        } catch (MathException e) {
            throw new MathException("The centers of the spheres are on one line");
        }

        // compute the coordinates of point H intersection of these four planes. (solve a linear system)
        Vector h = Plane.getIntersectingPointOfPlanes(q, p01, p12);
        assert Plane.isPointInPlane(h, p02);

        // get the normal vector U to Q from its equation (normalize a vector)
        Vector u = q.getNormalVector();

        // calculate the intersecting points of the line an one of the spheres (they all intersect in the same points, so it doesn't matter which sphere)
        Vector[] intersectingPoints = getIntersectingPointsOfSphereAndLine(s0, new Line(h, u));

        return intersectingPoints;
    }

    /**
     * Calculates the intersecting points of a line and a sphere
     *
     * @param sphere sphere intersected by the line
     * @param line   line intersecting the sphere
     * @return array of the intersecting points
     */
    private static Vector[] getIntersectingPointsOfSphereAndLine(Sphere sphere, Line line) throws MathException {
        // line is set through: (a = line.origin, v = line.direction)
        // a+t*v=0
        // ->	X = a0+t*v0
        // ->	Y = a1+t*v1
        // ->	Z = a2+t*v2
        //
        // insert line into sphere equation
        // X^2 + Y^2 + Z^2 + aX + bY + c*Z + d = 0
        // (a0+t*v0)^2 + (a1+t*v1)^2 + (a2+t*v2)^2 + a*(a0+t*v0) + b*(a1+t*v1) + c*(a2+t*v2) + d = 0
        //
        // solve after t:
        // t^2 + (a*v0 + b*v1 + c*v2 + 2*a0*v0 + 2*a1*v1 + 2*a2*v2)/(v0^2 + v1^2 + v2^2) * t + (d + c*a2 + b*a1 + a*a0 + a0^2 + a1^2 + a2^2)/(v0^2 + v1^2 + v2^2)= 0
        //
        // p = (a*v0 + b*v1 + c*v2 + 2*a0*v0 + 2*a1*v1 + 2*a2*v2+)/(v0^2 + v1^2 + v2^2)
        // q = (d + c*a2 + b*a1 + a*a0 + a0^2 + a1^2 + a2^2)/(v0^2 + v1^2 + v2^2)
        double quadraticP = (sphere.a * line.getDirection().getX() + sphere.b * line.getDirection().getY() + sphere.c * line.getDirection().getZ() + 2 * line.getOrigin().getX() * line.getDirection().getX() + 2 * line.getOrigin().getY() * line.getDirection().getY() + 2 * line.getOrigin().getZ() * line.getDirection().getZ()) / (Math.pow(line.getDirection().getX(), 2) + Math.pow(line.getDirection().getY(), 2) + Math.pow(line.getDirection().getZ(), 2));
        double quadraticQ = (sphere.d + sphere.c * line.getOrigin().getZ() + sphere.b * line.getOrigin().getY() + sphere.a * line.getOrigin().getX() + Math.pow(line.getOrigin().getX(), 2) + Math.pow(line.getOrigin().getY(), 2) + Math.pow(line.getOrigin().getZ(), 2)) / (Math.pow(line.getDirection().getX(), 2) + Math.pow(line.getDirection().getY(), 2) + Math.pow(line.getDirection().getZ(), 2));

        // calculate the ts that define the intersecting points
        // t1,2 = -p/2 +- ((p/2)^2 - q)^(1/2)
        double[] t;
        try {
            t = solveQuadratic(quadraticP, quadraticQ);
        } catch (MathException e) {
            throw new MathException("Line does not intersect sphere");
        }

        // calculate the points by defined through the ts
        Vector[] result = new Vector[2];
        for (int i = 0; i < 2; i++) {
            double x = line.getOrigin().getX() + t[i] * line.getDirection().getX();
            double y = line.getOrigin().getY() + t[i] * line.getDirection().getY();
            double z = line.getOrigin().getZ() + t[i] * line.getDirection().getZ();
            result[i] = new Vector(x, y, z);
        }
        return result;
    }

    /**
     * solves a quadratic formula in the from:<br>
     * x^2 + p*x + q = 0
     * x1,2 = -p/2 +- ((p/2)^2 - q)^(1/2)
     *
     * @param p
     * @param q
     * @return
     */
    private static double[] solveQuadratic(double p, double q) throws MathException {
        double[] x = new double[2];
        double delta = Math.sqrt(Math.pow((p / 2), 2) - q);
        if (Double.isNaN(delta)) {
            throw new MathException("negative value in square root");
        }
        x[0] = -p / 2 + delta;
        x[1] = -p / 2 - delta;
        return x;
    }

    public Vector getCenter() {
        return center;
    }

    public double getRadius() {
        return radius;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }

    public double getD() {
        return d;
    }

    public double getDistanceTo(Sphere s) {
        return Math.sqrt(Math.pow(this.center.getX() - s.center.getX(), 2)
                + Math.pow(this.center.getY() - s.center.getY(), 2)
                + Math.pow(this.center.getZ() - s.center.getZ(), 2));
    }

    public boolean isCollidingWith(Sphere s) {
        return this.getDistanceTo(s) <= (this.radius + s.radius);
    }

    @Override
    public String toString() {
        return "Sphere(center: " + center + ", radius: " + radius + ")";
    }
}
