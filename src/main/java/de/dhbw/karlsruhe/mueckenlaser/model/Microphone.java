package de.dhbw.karlsruhe.mueckenlaser.model;

import java.util.ArrayList;
import java.util.List;

public class Microphone {
    private List<FrequencyAmplitude> frequencyAmplitudes = new ArrayList<>();
    private double noiseIntensity = 0;

    public List<FrequencyAmplitude> getFrequencyAmplitudes() {
        return frequencyAmplitudes;
    }

    public void setFrequencyAmplitudes(List<FrequencyAmplitude> frequencyAmplitudes) {
        this.frequencyAmplitudes = frequencyAmplitudes;
    }

    public double getNoiseIntensity() {
        return noiseIntensity;
    }

    public void setNoiseIntensity(double noiseIntensity) {
        this.noiseIntensity = noiseIntensity;
    }
}
