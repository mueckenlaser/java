package de.dhbw.karlsruhe.mueckenlaser.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class EspStatus {
    public int getPitch() {
        return pitch;
    }

    public int getPitchControl() {
        return pitchControl;
    }

    public int getYaw() {
        return yaw;
    }

    public int getYawControl() {
        return yawControl;
    }

    /**
     * Current Pitch of ESP
     */
    @JsonProperty("pitch")
    private final int pitch;

    /**
     * Current Pitch Control value of ESP
     * This is the value the ESP tries to reach
     */
    @JsonProperty("pitch_control")
    private final int pitchControl;

    /**
     * Current Yaw of ESP
     */
    @JsonProperty("yaw")
    private final int yaw;

    /**
     * Current Yaw Control value of ESP
     * This is the value the ESP tries to reach
     */
    @JsonProperty("yaw_control")
    private final int yawControl;

    /**
     * Did the ESPs last request reach the master
     */
    @JsonProperty("reaching_master")
    private final boolean reachingMaster;

    /**
     * Did the last request to the ESP reach it
     */
    @JsonProperty("reachable_by_master")
    private final boolean reachableByMaster;

    @JsonCreator
    public EspStatus(int pitch, int pitchControl, int yaw, int yawControl, boolean reachingMaster) {
        this(pitch, pitchControl, yaw, yawControl, reachingMaster, true);
    }

    public EspStatus(int pitch, int pitchControl, int yaw, int yawControl, boolean reachingMaster, boolean reachableByMaster) {
        this.pitch = pitch;
        this.pitchControl = pitchControl;
        this.yaw = yaw;
        this.yawControl = yawControl;
        this.reachingMaster = reachingMaster;
        this.reachableByMaster = reachableByMaster;
    }

    public EspStatus() {
        this(0, 0, 0, 0, false, false);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EspStatus espStatus = (EspStatus) o;
        return pitch == espStatus.pitch &&
                pitchControl == espStatus.pitchControl &&
                yaw == espStatus.yaw &&
                yawControl == espStatus.yawControl &&
                reachingMaster == espStatus.reachingMaster &&
                reachableByMaster == espStatus.reachableByMaster;
    }

    @Override
    public int hashCode() {
        return Objects.hash(pitch, pitchControl, yaw, yawControl, reachingMaster, reachableByMaster);
    }
}
