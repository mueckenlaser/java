package de.dhbw.karlsruhe.mueckenlaser.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class LaserControl {
    /**
     * Pitch the ESP should turn to
     */
    @JsonProperty("pitch")
    private final int pitch;

    /**
     * Yaw the ESP should turn to
     */
    @JsonProperty("yaw")
    private final int yaw;


    @JsonCreator
    public LaserControl(int pitch, int yaw) {
        this.pitch = pitch;
        this.yaw = yaw;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LaserControl laserControl = (LaserControl) o;
        return pitch == laserControl.pitch &&
                yaw == laserControl.yaw;
    }

    @Override
    public int hashCode() {
        return Objects.hash(pitch, yaw);
    }

    public String toString() {
        return "pitch: " + pitch + " yaw: " + yaw;
    }
}
