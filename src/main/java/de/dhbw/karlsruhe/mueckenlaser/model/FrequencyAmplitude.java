package de.dhbw.karlsruhe.mueckenlaser.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

/**
 * Class for Tuples of frequency and amplitude
 */
public class FrequencyAmplitude {

    /**
     * Frequency in Hz
     */
    @JsonProperty("frequency")
    public final float frequency;

    /**
     * Amplitude
     */
    @JsonProperty("amplitude")
    public final float amplitude;

    public FrequencyAmplitude(float frequency, float amplitude) {
        this.frequency = frequency;
        this.amplitude = amplitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FrequencyAmplitude frequencyAmplitude = (FrequencyAmplitude) o;
        return amplitude == frequencyAmplitude.amplitude &&
                frequency == frequencyAmplitude.frequency;
    }

    @Override
    public int hashCode() {
        return Objects.hash(frequency, amplitude);
    }
}
