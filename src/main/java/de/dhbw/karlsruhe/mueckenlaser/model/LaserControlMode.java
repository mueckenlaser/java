package de.dhbw.karlsruhe.mueckenlaser.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class LaserControlMode {
    private final String CONTROL_MODE_JSON_KEY = "control_mode";

    @JsonProperty(CONTROL_MODE_JSON_KEY)
    private final LaserControlModeEnum controlMode;

    private final LaserControlModeEnum defaultControlMode = LaserControlModeEnum.MANUAL;

    @JsonCreator
    public LaserControlMode(@JsonProperty(CONTROL_MODE_JSON_KEY) String s) {
        this.controlMode = convertControlModeFromString(s);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LaserControlMode laserControlMode = (LaserControlMode) o;
        return controlMode == laserControlMode.controlMode;
    }

    @Override
    public String toString() {
        return controlMode.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(controlMode);
    }

    private LaserControlModeEnum convertControlModeFromString(String string) {
        try{
            return LaserControlModeEnum.valueOf(string);
        } catch (IllegalArgumentException ignored) {
            return defaultControlMode;
        }
    }

    private enum LaserControlModeEnum {
        MANUAL, MICROPHONE
    }
}
