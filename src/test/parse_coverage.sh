#!/bin/sh

FILE="target/site/jacoco/jacoco.csv"

if test -f "$FILE"; then
  awk -F"," '{ instructions += $4 + $5; covered += $5 } END { print "[\033[1;32mCOVERAGE\033[0m]", covered, "/", instructions, "instructions covered"; print "[\033[1;32mCOVERAGE\033[0m]", 100*covered/instructions, "% covered" }' "$FILE"
else
  awk 'BEGIN { print "[\033[1;32mCOVERAGE\033[0m] No test coverage csv found" }'
fi
