package de.dhbw.karlsruhe.mueckenlaser.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureRestDocs(outputDir = "target/snippets")
public class PagesControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void indexTest() throws Exception {
        this.mockMvc.perform(get("/")).andExpect(status().isOk())
                .andExpect(content().string(containsString("Welcome to Mueckenlaser")))
                .andExpect(content().string(containsString("navbar navbar-expand-lg navbar-light")))
                .andExpect(content().string(containsString("<title>Mueckenlaser</title>")))
                .andDo(document("home"));
    }

    @Test
    public void espDummyTest() throws Exception {
        this.mockMvc.perform(get("/esp_dummy")).andExpect(status().isOk())
                .andExpect(content().string(containsString("<img height=\"290\" src=\"https://github.com/mueckenlaser/images/raw/dd7154703fb00ab4d2b6b89e0b3c809e10b7d58e/logo.png\" width=\"290\">")))
                .andExpect(content().string(containsString("<td>Pitch</td>")))
                .andExpect(content().string(containsString("<td>Yaw Control</td>")))
                .andDo(document("esp_dummy"));
    }

    @Test
    public void laserTest() throws Exception {
        this.mockMvc.perform(get("/laser")).andExpect(status().isOk())
                .andExpect(content().string(containsString("<title>Laser</title>")))
                .andExpect(content().string(containsString("<h4 class=\"h4 card-header\"><b>Laser Status</b> <span class=\"badge badge-pill badge-danger\" id=\"esp_connection_status\">DISCONNECTED</span></h4>")))
                .andExpect(content().string(containsString("<h4 class=\"h4 card-header\"><b>Laser Control</b></h4>")))
                .andExpect(content().string(containsString("<h4 class=\"h4 card-header\"><b>Laser Visualization</b></h4>")))
                .andDo(document("laser"));
    }

    @Test
    public void microphoneTest() throws Exception {
        this.mockMvc.perform(get("/microphone")).andExpect(status().isOk())
                .andExpect(content().string(containsString("<title>Microphone Graph</title>")))
                .andExpect(content().string(containsString("<canvas height=\"300\" id=\"microphoneChart0\" width=\"1600\"></canvas>")))
                .andExpect(content().string(containsString("<canvas height=\"300\" id=\"microphoneChart1\" width=\"1600\"></canvas>")))
                .andExpect(content().string(containsString("<canvas height=\"300\" id=\"microphoneChart2\" width=\"1600\"></canvas>")))
                .andDo(document("microphone"));
    }
}
