package de.dhbw.karlsruhe.mueckenlaser.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureRestDocs(outputDir = "target/snippets")
public class LaserRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getControlsTest() throws Exception {
        this.mockMvc.perform(get("/laser/controls").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(document("laser/controls/get", preprocessResponse(prettyPrint())));
    }

    @Test
    public void getControlModeTest() throws Exception {
        this.mockMvc.perform(get("/laser/control_mode").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(document("laser/control_mode/get", preprocessResponse(prettyPrint())));
    }

    @Test
    public void getLaserStatusTest() throws Exception {
        this.mockMvc.perform(get("/laser/status").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(document("laser/status/get", preprocessResponse(prettyPrint())));
    }

    @Test
    public void getDummyStatusTest() throws Exception {
        this.mockMvc.perform(get("/status").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(document("esp/dummy/get", preprocessResponse(prettyPrint())));
    }
}
