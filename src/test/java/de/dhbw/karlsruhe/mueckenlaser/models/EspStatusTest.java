package de.dhbw.karlsruhe.mueckenlaser.models;

import de.dhbw.karlsruhe.mueckenlaser.model.EspStatus;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class EspStatusTest {
    Random r = new Random();

    @Test
    public void newEspStatusEmptyConstructor_createdSuccessfully() {
        EspStatus test = new EspStatus();
        assertEquals(test.getClass(), EspStatus.class);
    }

    @Test
    public void newEspStatusJSONConstructor_createdSuccessfully() {
        EspStatus test = new EspStatus(r.nextInt(), r.nextInt(), r.nextInt(), r.nextInt(), r.nextBoolean(), r.nextBoolean());
        assertEquals(test.getClass(), EspStatus.class);
    }

    @Test
    public void espStatusEqualsAnotherWithSameParameters() {
        int pitch = r.nextInt();
        int pitchControl = r.nextInt();
        int yaw = r.nextInt();
        int yawControl = r.nextInt();
        boolean reachingMaster = r.nextBoolean();
        boolean reachableByMaster = r.nextBoolean();

        EspStatus a = new EspStatus(pitch, pitchControl, yaw, yawControl, reachingMaster, reachableByMaster);
        EspStatus b = new EspStatus(pitch, pitchControl, yaw, yawControl, reachingMaster, reachableByMaster);

        assertEquals(a, b);
        assertEquals(a.hashCode(), b.hashCode());
    }

    @Test
    public void espStatusDiffersFromAnotherWithDifferentPitch() {
        int pitch = r.nextInt();
        int pitchControl = r.nextInt();
        int yaw = r.nextInt();
        int yawControl = r.nextInt();
        boolean reachingMaster = r.nextBoolean();
        boolean reachableByMaster = r.nextBoolean();

        EspStatus a = new EspStatus(pitch, pitchControl, yaw, yawControl, reachingMaster, reachableByMaster);
        EspStatus b = new EspStatus(pitch + 1, pitchControl, yaw, yawControl, reachingMaster, reachableByMaster);

        assertNotEquals(a, b);
        assertNotEquals(a.hashCode(), b.hashCode());
    }

    @Test
    public void espStatusDiffersFromAnotherWithDifferentPitchControl() {
        int pitch = r.nextInt();
        int pitchControl = r.nextInt();
        int yaw = r.nextInt();
        int yawControl = r.nextInt();
        boolean reachingMaster = r.nextBoolean();
        boolean reachableByMaster = r.nextBoolean();

        EspStatus a = new EspStatus(pitch, pitchControl, yaw, yawControl, reachingMaster, reachableByMaster);
        EspStatus b = new EspStatus(pitch, pitchControl + 1, yaw, yawControl, reachingMaster, reachableByMaster);

        assertNotEquals(a, b);
        assertNotEquals(a.hashCode(), b.hashCode());
    }

    @Test
    public void espStatusDiffersFromAnotherWithDifferentYaw() {
        int pitch = r.nextInt();
        int pitchControl = r.nextInt();
        int yaw = r.nextInt();
        int yawControl = r.nextInt();
        boolean reachingMaster = r.nextBoolean();
        boolean reachableByMaster = r.nextBoolean();

        EspStatus a = new EspStatus(pitch, pitchControl, yaw, yawControl, reachingMaster, reachableByMaster);
        EspStatus b = new EspStatus(pitch, pitchControl, yaw + 1, yawControl, reachingMaster, reachableByMaster);

        assertNotEquals(a, b);
        assertNotEquals(a.hashCode(), b.hashCode());
    }

    @Test
    public void espStatusDiffersFromAnotherWithDifferentYawControl() {
        int pitch = r.nextInt();
        int pitchControl = r.nextInt();
        int yaw = r.nextInt();
        int yawControl = r.nextInt();
        boolean reachingMaster = r.nextBoolean();
        boolean reachableByMaster = r.nextBoolean();

        EspStatus a = new EspStatus(pitch, pitchControl, yaw, yawControl, reachingMaster, reachableByMaster);
        EspStatus b = new EspStatus(pitch, pitchControl, yaw, yawControl + 1, reachingMaster, reachableByMaster);

        assertNotEquals(a, b);
        assertNotEquals(a.hashCode(), b.hashCode());
    }

    @Test
    public void espStatusDiffersFromAnotherWithDifferentReachingMaster() {
        int pitch = r.nextInt();
        int pitchControl = r.nextInt();
        int yaw = r.nextInt();
        int yawControl = r.nextInt();
        boolean reachingMaster = r.nextBoolean();
        boolean reachableByMaster = r.nextBoolean();

        EspStatus a = new EspStatus(pitch, pitchControl, yaw, yawControl, reachingMaster, reachableByMaster);
        EspStatus b = new EspStatus(pitch, pitchControl, yaw, yawControl, !reachingMaster, reachableByMaster);

        assertNotEquals(a, b);
        assertNotEquals(a.hashCode(), b.hashCode());
    }

    @Test
    public void espStatusDiffersFromAnotherWithDifferentReachableByMaster() {
        int pitch = r.nextInt();
        int pitchControl = r.nextInt();
        int yaw = r.nextInt();
        int yawControl = r.nextInt();
        boolean reachingMaster = r.nextBoolean();
        boolean reachableByMaster = r.nextBoolean();

        EspStatus a = new EspStatus(pitch, pitchControl, yaw, yawControl, reachingMaster, reachableByMaster);
        EspStatus b = new EspStatus(pitch, pitchControl, yaw, yawControl, reachingMaster, !reachableByMaster);

        assertNotEquals(a, b);
        assertNotEquals(a.hashCode(), b.hashCode());
    }
}
