package de.dhbw.karlsruhe.mueckenlaser.models;

import de.dhbw.karlsruhe.mueckenlaser.model.FrequencyAmplitude;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class FrequencyAmplitudeTest {
    Random r = new Random();

    @Test
    public void newFrequencyAmplitudeConstructor_createdSuccessfully() {
        FrequencyAmplitude test = new FrequencyAmplitude(r.nextFloat(), r.nextFloat());
        assertEquals(test.getClass(), FrequencyAmplitude.class);
    }

    @Test
    public void frequencyAmplitudeEqualsAnotherWithSameParameters() {
        float frequency = r.nextFloat();
        float amplitude = r.nextFloat();

        FrequencyAmplitude a = new FrequencyAmplitude(frequency, amplitude);
        FrequencyAmplitude b = new FrequencyAmplitude(frequency, amplitude);

        assertEquals(a, b);
        assertEquals(a.hashCode(), b.hashCode());
    }

    @Test
    public void frequencyAmplitudeDiffersFromAnotherWithDifferentFrequency() {
        float frequency = r.nextFloat();
        float amplitude = r.nextFloat();

        FrequencyAmplitude a = new FrequencyAmplitude(frequency, amplitude);
        FrequencyAmplitude b = new FrequencyAmplitude(frequency + 1, amplitude);

        assertNotEquals(a, b);
        assertNotEquals(a.hashCode(), b.hashCode());
    }

    @Test
    public void frequencyAmplitudeDiffersFromAnotherWithDifferentAmplitude() {
        float frequency = r.nextFloat();
        float amplitude = r.nextFloat();

        FrequencyAmplitude a = new FrequencyAmplitude(frequency, amplitude);
        FrequencyAmplitude b = new FrequencyAmplitude(frequency, amplitude + 1);

        assertNotEquals(a, b);
        assertNotEquals(a.hashCode(), b.hashCode());
    }
}
