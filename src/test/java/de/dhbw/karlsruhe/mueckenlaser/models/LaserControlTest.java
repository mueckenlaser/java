package de.dhbw.karlsruhe.mueckenlaser.models;

import de.dhbw.karlsruhe.mueckenlaser.model.LaserControl;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class LaserControlTest {
    Random r = new Random();

    @Test
    public void newLaserControlModeFullConstructor_createdSuccessfully() {
        LaserControl test = new LaserControl(r.nextInt(), r.nextInt());
        assertEquals(test.getClass(), LaserControl.class);
    }

    @Test
    public void laserControlModeEqualsAnotherWithSameParameters() {
        int pitch = r.nextInt();
        int yaw = r.nextInt();

        LaserControl a = new LaserControl(pitch, yaw);
        LaserControl b = new LaserControl(pitch, yaw);

        assertEquals(a, b);
        assertEquals(a.hashCode(), b.hashCode());
    }

    @Test
    public void laserControlModeDiffersFromAnotherWithDifferentPitch() {
        int pitch = r.nextInt();
        int yaw = r.nextInt();

        LaserControl a = new LaserControl(pitch, yaw);
        LaserControl b = new LaserControl(pitch + 1, yaw);

        assertNotEquals(a, b);
        assertNotEquals(a.hashCode(), b.hashCode());
    }

    @Test
    public void laserControlModeDiffersFromAnotherWithDifferentYaw() {
        int pitch = r.nextInt();
        int yaw = r.nextInt();

        LaserControl a = new LaserControl(pitch, yaw);
        LaserControl b = new LaserControl(pitch, yaw + 1);

        assertNotEquals(a, b);
        assertNotEquals(a.hashCode(), b.hashCode());
    }

}
