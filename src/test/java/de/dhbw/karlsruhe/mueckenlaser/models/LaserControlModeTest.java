package de.dhbw.karlsruhe.mueckenlaser.models;

import de.dhbw.karlsruhe.mueckenlaser.model.LaserControlMode;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class LaserControlModeTest {
    @Test
    public void newLaserControlModeConstructor_createdSuccessfully() {
        LaserControlMode test = new LaserControlMode("MICROPHONE");
        assertEquals(test.getClass(), LaserControlMode.class);
    }

    @Test
    public void laserControlModeEqualsAnotherWithSameParameters() {
        LaserControlMode a = new LaserControlMode("MANUAL");
        LaserControlMode b = new LaserControlMode("MANUAL");

        assertEquals(a, b);
        assertEquals(a.hashCode(), b.hashCode());
    }

    @Test
    public void laserControlModeDefaultsToManual() {
        LaserControlMode a = new LaserControlMode("asdf");
        LaserControlMode b = new LaserControlMode("MANUAL");
        LaserControlMode c = new LaserControlMode("MICROPHONE");

        assertEquals(a, b);
        assertEquals(a.hashCode(), b.hashCode());

        assertNotEquals(a, c);
        assertNotEquals(a.hashCode(), c.hashCode());
    }

    @Test
    public void laserControlModeDiffersFromAnotherWithDifferentMode() {
        LaserControlMode a = new LaserControlMode("MANUAL");
        LaserControlMode b = new LaserControlMode("MICROPHONE");

        assertNotEquals(a, b);
        assertNotEquals(a.hashCode(), b.hashCode());
    }
}
