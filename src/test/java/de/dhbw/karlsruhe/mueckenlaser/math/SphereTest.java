package de.dhbw.karlsruhe.mueckenlaser.math;

import de.dhbw.karlsruhe.mueckenlaser.localization.math.Sphere;
import de.dhbw.karlsruhe.mueckenlaser.localization.math.Vector;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class SphereTest {
    @Test
    public void getIntersectingPoints_twoSpheresOnlyTouch_singleIntersectionPoint() throws Exception {
        Sphere s0 = new Sphere(new Vector(1, 0, 0), 1);
        Sphere s1 = new Sphere(new Vector(0, 1, 0), 1);
        Sphere s2 = new Sphere(new Vector(0, -1, 0), 1);
        Vector[] result = Sphere.getIntersectingPoints(s0, s1, s2);

        Vector[] expected = {new Vector(0, 0, 0), new Vector(0, 0, 0)};
        String message = "the intersection points should both be in (0, 0, 0)";
        assertArrayEquals(expected, result, message);
    }

    @Test
    public void getIntersectingPoints_allSpheresIntersect_twoIntersectionPoints() throws Exception {
        Sphere s0 = new Sphere(new Vector(1, 0, 0), 1);
        Sphere s1 = new Sphere(new Vector(0, 1, 0), 1);
        Sphere s2 = new Sphere(new Vector(0, 0, 1), 1);
        Vector[] result = Sphere.getIntersectingPoints(s0, s1, s2);

        Vector[] expected = {new Vector(0.6666666666666666, 0.6666666666666666, 0.6666666666666666), new Vector(0, 0, 0)};
        String message = "the intersection points should both be in (0, 0, 0)";
        assertArrayEquals(expected, result, message);
    }

    @Test
    public void getIntersectingPoints_noIntersectionPoint_exception() {
        Sphere s0 = new Sphere(new Vector(2, 0, 0), 1);
        Sphere s1 = new Sphere(new Vector(0, 2, 0), 1);
        Sphere s2 = new Sphere(new Vector(0, -2, 0), 1);

        Exception e = assertThrows(Exception.class, () -> Sphere.getIntersectingPoints(s0, s1, s2));
        String expected = "The spheres don't intersect";
        String message = "Should throw Exception(\"" + expected + "\")";
        assertEquals(expected, e.getMessage(), message);
    }

    @Test
    public void getIntersectingPoints_spheresOnOneLine_exception() {
        Sphere s0 = new Sphere(new Vector(0, 0, 0), 1);
        Sphere s1 = new Sphere(new Vector(1, 0, 0), 1);
        Sphere s2 = new Sphere(new Vector(2, 0, 0), 1);

        Exception e = assertThrows(Exception.class, () -> Sphere.getIntersectingPoints(s0, s1, s2));
        String expected = "The centers of the spheres are on one line";
        String message = "Should throw Exception(\"" + expected + "\")";
        assertEquals(expected, e.getMessage(), message);
    }
}
