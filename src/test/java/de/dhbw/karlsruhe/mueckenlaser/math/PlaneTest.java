package de.dhbw.karlsruhe.mueckenlaser.math;

import de.dhbw.karlsruhe.mueckenlaser.localization.math.Plane;
import de.dhbw.karlsruhe.mueckenlaser.localization.math.Sphere;
import de.dhbw.karlsruhe.mueckenlaser.localization.math.Vector;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PlaneTest {
    @Test
    public void getNormalVector_successful() {
        Plane p = new Plane(12, 23, 34, 45);
        Vector result = p.getNormalVector();
        Vector expected = new Vector(12, 23, 34);
        String message = "normal vector should be (12, 23, 34)";
        assertEquals(expected, result, message);
    }

    @Test
    public void getIntersectingPlaneBetweenSpheres_spheresHaveDifferentCenter_correctPlane() throws Exception {
        Sphere s0 = new Sphere(new Vector(0, 0, 0), 1);
        Sphere s1 = new Sphere(new Vector(1, 0, 0), 1);
        Plane result = Plane.getIntersectingPlaneBetweenSpheres(s0, s1);
        Plane expected = new Plane(1, 0, 0, -0.5);
        String message = "Plane should be 1x + 0y + 0z - 0.5 = 0";
        assertEquals(expected, result, message);
    }

    @Test
    public void getIntersectingPlaneBetweenSpheres_spheresHaveSameCenter_exception() {
        Sphere s0 = new Sphere(new Vector(0, 0, 0), 1);
        Sphere s1 = new Sphere(new Vector(0, 0, 0), 1);
        Exception e = assertThrows(Exception.class, () -> Plane.getIntersectingPlaneBetweenSpheres(s0, s1));
        String expected = "Cannot create intersecting plane, because spheres have the same center";
        String message = "Should throw Exception(\"" + expected + "\")";
        assertEquals(expected, e.getMessage(), message);
    }

    @Test
    public void getIntersectingPlaneBetweenSpheres_spheresDontIntersect_exception() {
        Sphere s0 = new Sphere(new Vector(0, 0, 0), 1);
        Sphere s1 = new Sphere(new Vector(5, 0, 0), 1);
        Exception e = assertThrows(Exception.class, () -> Plane.getIntersectingPlaneBetweenSpheres(s0, s1));
        String expected = "Cannot create intersecting plane, because spheres dont intersect";
        String message = "Should throw Exception(\"" + expected + "\")";
        assertEquals(expected, e.getMessage(), message);
    }

    @Test
    public void getIntersectingPointOfPlanes_allPlanesAreDifferent_correctIntersectingPoints() throws Exception {
        Plane p0 = new Plane(1, 0, 0, 0);
        Plane p1 = new Plane(0, 1, 0, 0);
        Plane p2 = new Plane(0, 0, 1, 0);
        Vector result = Plane.getIntersectingPointOfPlanes(p0, p1, p2);
        Vector expected = new Vector(0, 0, 0);
        String message = "Intersecting point should be (0, 0, 0)";
        assertEquals(expected, result, message);
    }

    @Test
    public void getIntersectingPointOfPlanes_twoPlanesAreParallel_exception() {
        Plane p0 = new Plane(1, 0, 0, 0);
        Plane p1 = new Plane(2, 0, 0, 1);
        Plane p2 = new Plane(0, 1, 0, 0);
        Exception e = assertThrows(Exception.class, () -> Plane.getIntersectingPointOfPlanes(p0, p1, p2));
        String expected = "The planes don't have a intersection point in common because two planes are parallel";
        String message = "Should throw Exception(\"" + expected + "\")";
        assertEquals(expected, e.getMessage(), message);
    }

    @Test
    public void isPointInPlane_true() {
        Vector point = new Vector(0, 0, 0);
        Plane plane = new Plane(1, 0, 0, 0);
        assertTrue(Plane.isPointInPlane(point, plane));
    }

    @Test
    public void isPointInPlane_false() {
        Vector point = new Vector(0, 0, 0);
        Plane plane = new Plane(1, 0, 0, 1);
        assertFalse(Plane.isPointInPlane(point, plane));
    }

    @Test
    public void getPlaneFromPoints_threeDifferentPoints_correctPlane() throws Exception {
        Vector p0 = new Vector(0, 0, 0);
        Vector p1 = new Vector(1, 0, 0);
        Vector p2 = new Vector(0, 1, 0);
        Plane result = Plane.getPlaneFromPoints(p0, p1, p2);
        Plane expected = new Plane(0, 0, 1, 0);
        String message = "Plane should be 0x + 0y + 1z + 0 = 0";
        assertEquals(expected, result, message);
    }

    @Test
    public void getPlaneFromPoints_allPointsAreOnOneLine_exception() {
        Vector p0 = new Vector(0, 0, 0);
        Vector p1 = new Vector(1, 0, 0);
        Vector p2 = new Vector(2, 0, 0);
        Exception e = assertThrows(Exception.class, () -> Plane.getPlaneFromPoints(p0, p1, p2));
        String expected = "Cannot create Plane because all three points are on one line";
        String message = "Should throw Exception(\"" + expected + "\")";
        assertEquals(expected, e.getMessage(), message);
    }

    @Test
    void equals_equalPlanes_true() {
        Plane p0 = new Plane(1, 2, 3, 4);
        Plane p1 = new Plane(1, 2, 3, 4);
        assertEquals(p0, p1);
    }

    @Test
    void equals_p1IsMultipleOfp0_true_1() {
        Plane p0 = new Plane(1, -2, 3, 4);
        Plane p1 = new Plane(2, -4, 6, 8);
        assertEquals(p0, p1);
    }

    @Test
    void equals_p1IsMultipleOfp0_true_2() {
        Plane p0 = new Plane(0, -2, 3, 4);
        Plane p1 = new Plane(0, -4, 6, 8);
        assertEquals(p0, p1);
    }

    @Test
    void equals_p1IsMultipleOfp0_true_3() {
        Plane p0 = new Plane(0, 0, 3, 4);
        Plane p1 = new Plane(0, 0, 6, 8);
        assertEquals(p0, p1);
    }

    @Test
    void equals_PlanesNotEqual_false() {
        Plane p0 = new Plane(0, 0, 1, 1);
        Plane p1 = new Plane(0, 0, 1, 2);
        assertNotEquals(p0, p1);
    }

    @Test
    void toString_() {
        Plane p = new Plane(1, 0, 2, -4);
        String expected = "Plane: 1.0x + 0.0y + 2.0z + -4.0 = 0";
        String actual = p.toString();
        assertEquals(expected, actual);
    }
}
