package de.dhbw.karlsruhe.mueckenlaser.math;

import de.dhbw.karlsruhe.mueckenlaser.localization.math.Vector;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class VectorTest {
    @Test
    public void getDirectionVector_successful() {
        Vector result = Vector.getDirectionVector(new Vector(12, 34, 56), new Vector(234, 987, 456));
        Vector expected = new Vector(222, 953, 400);
        String message = "direction vector should be (222, 953, 400)";
        assertEquals(expected, result, message);
    }

    @Test
    public void getCrossProduct_successful() {
        Vector result = Vector.getCrossProduct(new Vector(26, 34, 93), new Vector(-12, 54, -32));
        Vector expected = new Vector(-6110, -284, 1812);
        String message = "cross product should be (-6110, -284, 1812)";
        assertEquals(expected, result, message);
    }
}
